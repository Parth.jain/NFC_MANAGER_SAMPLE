package com.example.parth_pc.readnfcdata;

import android.content.Context;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Context context=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NfcManager manager = (NfcManager) context.getSystemService(Context.NFC_SERVICE);
        NfcAdapter adapter = manager.getDefaultAdapter();
        if (adapter != null && adapter.isEnabled()) {
            Intent intent = getIntent();
            if (intent.getType() != null && intent.getType().equals("application/" + getPackageName())) {
                // Read the first record which contains the NFC data
                Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
                NdefRecord relayRecord = ((NdefMessage) rawMsgs[0]).getRecords()[0];
                String nfcData = new String(relayRecord.getPayload());

                // Display the data on the tag
                Toast.makeText(this, nfcData, Toast.LENGTH_SHORT).show();

                // Do other stuff with the data...

                // Just finish the activity
                finish();
            }
        }
        //Yes NFC available
        else

        {
            Toast.makeText(this, "NFC Feature Doesn't supported by device, Use app in another " +
                            "device", Toast.LENGTH_SHORT).show();
            //Your device doesn't support NFC
        }
    }
}
